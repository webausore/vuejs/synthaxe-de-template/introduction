# Vue JS - Synthaxe de template

---

## Détails

- Interpolations : https://fr.vuejs.org/v2/guide/syntax.html#Interpolations
- Directives : https://fr.vuejs.org/v2/guide/syntax.html#Directives
- Abréviations : https://fr.vuejs.org/v2/guide/syntax.html#Abreviations